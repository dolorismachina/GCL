﻿using System;
using SFML.Window;
using SFML.Graphics;
namespace GCL
{
    /// <summary>
    /// Base class for all screens (eg. Splash, Menu, Level etc.)
    /// It is responsible for updating and drawing of game objects that are included in the game's active screen.
    /// </summary>
    public abstract class Screen
    {
        public Screen(string title, Game parent)
        {
            Title = title;   
            Parent = parent;
        }

        public String Title { get; private set; }

        public Game Parent { get; private set; }

        public abstract void Update(double dt);
        public abstract void Render();

        /// <summary>
        /// Wraps Parent.Window.Draw() into single function.
        /// </summary>
        /// <param name="drawable">Object to be drawn.</param>
        protected void Draw(Drawable drawable)
        {
            Parent.Window.Draw(drawable);
        }

        protected bool isKeyPressed(Keyboard.Key key)
        {
            if (Keyboard.IsKeyPressed(key))
                return true;
            else
                return false;
        }

        protected bool isMouseButtonPressed(Mouse.Button button)
        {
            if (Mouse.IsButtonPressed(button))
                return true;
            else
                return false;
        }
    }
}