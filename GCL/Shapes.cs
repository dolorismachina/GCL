﻿using SFML.Graphics;
using SFML.Window;

namespace GCL
{
    /// <summary>
    /// Class for creating isosceles triangles.
    /// </summary>
    public class TriangleShape : Shape
    {
        public TriangleShape(float x, float y, float w, float h)
            : base()
        {
            size = new Vector2f(w, h);
            Position = new Vector2f(x, y);

            OutlineThickness = 1;
            OutlineColor = Color.Blue;
        }

        public TriangleShape(Vector2f position, Vector2f size)
            : this(position.X, position.Y, size.X, size.Y) { }

        public override Vector2f GetPoint(uint index)
        {
            switch (index)
            {
                default:
                case 0: return new Vector2f(0, size.Y); // Left point.
                case 1: return new Vector2f(size.X, size.Y); // Right point.
                case 2: return new Vector2f(size.X * 0.5f, 0); // Top point.
            }
        }

        public override uint GetPointCount()
        {
            return 3;
        }

        private Vector2f size;
    }
}
