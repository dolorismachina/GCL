﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCL
{
    public enum SpriteSheetType
    {
        OneSheetPerAnimation,
        OneRowPerAnimation,
        OneColumnPerAnimation
    }

    /// <summary>
    /// Sprite that is animated using spritesheets.
    /// </summary>
    public class AnimatedSprite : Sprite
    {
        /// <summary>
        /// Creates a new animation.
        /// </summary>
        /// <param name="frameSize">Width and height of a single frame.</param>
        /// <param name="source">The spritesheet that contains animation frames.</param>
        /// <param name="speed">How many frames of animation are displayed each second.</param>
        /// <param name="type">The way the frames are organized on the spritesheet.</param>
        public AnimatedSprite(Vector2i frameSize, Texture source, int speed, SpriteSheetType type)
        {
            this.frameSize = frameSize;
            Texture = source;
            this.type = type;

            lastFrameChange = DateTime.Now;
            millisecondsPerFrame = (int)(1000 / speed);

            TextureRect = new IntRect(0, 0, frameSize.X, frameSize.Y);

            framesInSheet = new Vector2i((int)(Texture.Size.X / frameSize.X), (int)(Texture.Size.Y / frameSize.Y));
            Origin = new Vector2f(TextureRect.Width * 0.5f, TextureRect.Height * 0.5f);
        }

        /// <summary>
        /// Animates the spritesheet.
        /// Based on the type of the spritesheet set during creation of the animation it calls appropriate function.
        /// </summary>
        public void Animate()
        {
            millisecondsSinceLastFrame = (int)Utilities.GetTimeDifference(lastFrameChange, true);
            if (millisecondsSinceLastFrame > millisecondsPerFrame)
            {
                lastFrameChange = DateTime.Now;

                switch (type)
                {
                    case SpriteSheetType.OneRowPerAnimation:
                        AnimateRow();
                        break; 

                    case SpriteSheetType.OneColumnPerAnimation:
                        AnimateColumn();
                        break;

                    case SpriteSheetType.OneSheetPerAnimation:
                        AnimateAll();
                        break;
                }
            }
        }

        /// <summary>
        /// Animates a spritesheet that has its frames in a row.
        /// </summary>
        private void AnimateRow()
        {
            if (frame == framesInSheet.X)
            {
                LastFrame = true;
                frame = 1;
            }
            else
            {
                frame++;
            }

            framePositionLeft = (frame - 1) * frameSize.X;
            framePosionTop = (int)(row - 1) * frameSize.Y;
            TextureRect = new IntRect(framePositionLeft, framePosionTop, frameSize.X, frameSize.Y);
        }

        /// <summary>
        /// Animates a spritesheet that has its frames in a column.
        /// </summary>
        private void AnimateColumn()
        {
            if (frame == framesInSheet.Y)
            {
                frame = 1;
            }
            else
            {
                frame++;
            }

            framePositionLeft = (int)(column - 1) * frameSize.X;
            framePosionTop = (frame - 1) * frameSize.Y;
            TextureRect = new IntRect(framePositionLeft, framePosionTop, frameSize.X, frameSize.Y);
        }

        // Only works if the spritesheet has the same number of colums and rows and all the fields are filled.
        // Eg. Spritesheet is 5x5 and has 25 frames. If it's 5x5 and has less than 25 frames this wont work as intender.
        // It will display all the empty frames that follow the last frame.
        /// <summary>
        /// Animates a spritesheet that has its frames in the whole spritesheet in a horizontal manner, going from left to right, top to bottom.
        /// </summary>
        private void AnimateAll()
        {
            if (row == framesInSheet.Y && column == framesInSheet.X)
            {
                LastFrame = true;
                row = 1;
                column = 1;
            }
            else if (column == framesInSheet.X)
            {
                column = 1;
                row++;
            }
            else
            {
                column++;
            }

            framePositionLeft = (int)(column - 1) * frameSize.X;
            framePosionTop = (int)(row - 1) * frameSize.Y;
            TextureRect = new IntRect(framePositionLeft, framePosionTop, frameSize.X, frameSize.Y);
        }

        /// <summary>
        /// Allows the user to change current animation in a column animation.
        /// </summary>
        public uint Column
        {
            get { return column; }
            set
            {
                if (value < 1)
                {
                    column = 1;

                    framePositionLeft = (int)(column - 1) * frameSize.X;
                    framePosionTop = (frame - 1) * frameSize.Y;
                    TextureRect = new IntRect(framePositionLeft, framePosionTop, frameSize.X, frameSize.Y);
                }
                else
                {
                    column = value;

                    framePositionLeft = (int)(column - 1) * frameSize.X;
                    framePosionTop = (frame - 1) * frameSize.Y;
                    TextureRect = new IntRect(framePositionLeft, framePosionTop, frameSize.X, frameSize.Y);
                }
            }
        }
        /// <summary>
        /// Allows the user to change current animation in a row animation.
        /// </summary>
        public uint Row
        {
            get { return row; }
            set
            {
                if (value < 1)
                {
                    row = 1;

                    framePositionLeft = (frame - 1) * frameSize.X;
                    framePosionTop = (int)(row - 1) * frameSize.Y;
                    TextureRect = new IntRect(framePositionLeft, framePosionTop, frameSize.X, frameSize.Y);
                }
                else
                {
                    row = value;

                    framePositionLeft = (frame - 1) * frameSize.X;
                    framePosionTop = (int)(row - 1) * frameSize.Y;
                    TextureRect = new IntRect(framePositionLeft, framePosionTop, frameSize.X, frameSize.Y);
                }
            }
        }

        public bool LastFrame { get; private set; }
        private SpriteSheetType type;
        private Vector2i frameSize; // Width and height of one frame expressed in pixels.

        private Vector2i framesInSheet; // Holds number of frames in each column and row starting with 1.

        private DateTime lastFrameChange; // Holds time of last frame change.

        private int millisecondsPerFrame; // Minimum umber of milliseconds that need to pass before next frame is displayed.
        private int millisecondsSinceLastFrame; // Number of milliseconds since the frame was changed.

        private int frame = 1; // Current animation frame.
        private uint column = 1; // Current column.
        private uint row = 1; // Current row.

        private int framePositionLeft = 0;
        private int framePosionTop = 0;
    }
}
